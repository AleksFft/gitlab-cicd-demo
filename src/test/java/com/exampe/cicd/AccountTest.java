package com.exampe.cicd;

import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

public class AccountTest {

    @Test
    public void foo_equals_hello() {
        assertEquals(Account.foo(), "hello");
    }

    @Test
    public void foo_notEquals_world() {
        assertNotEquals(Account.foo(), "world");
    }

    @Test
    public void foo_isNot_null() {
        assertTrue(Objects.nonNull(Account.foo()));
    }


}